﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LogView : MonoBehaviour {
    public static string _logs = "";
    private string _oldLogs = "";
    private ScrollRect _scrollRect;
    private Text _textLog;

    // Use this for initialization
    private void Start() {
        StopAllCoroutines();
        _scrollRect = gameObject.GetComponent<ScrollRect>();
        _textLog = _scrollRect.content.GetComponentInChildren<Text>();
    }

    // Update is called once per frame
    private void Update() {
        if (_scrollRect != null && _logs != _oldLogs) {
            _textLog.text = _logs;
            StartCoroutine(DelayMethod(5, () => {
                _scrollRect.verticalNormalizedPosition = 0;
            }));
            _oldLogs = _logs;
        }
	}

    public static void Clear() {
        _logs = "";
    }

    public static void Log(string logText) {
        _logs += DateTime.Now.ToString("hh:mm:ss ") + logText + "\n";
        Debug.Log(_logs);
    }

    private IEnumerator DelayMethod(int delayCount, Action action) {
        for (int i=0; i<delayCount; ++i) {
            yield return null;
        }
        action();
    }

    public void StopLogView() {
        StopCoroutine("DelayMethod");
    }
}
