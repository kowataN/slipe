﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugView : MonoBehaviour
{
    [Header("View")]
    [SerializeField] private GameView _GameView = default;
    //[SerializeField] MatchingView _MatchingView = default;
    [SerializeField] private InitPosView _InitPosView = default;

    [Header("Texts")]
    //[SerializeField] private GameObject _Field = default;
    [SerializeField] private Text _TxtField = default;
    [SerializeField] private Text _TxtMatching = default;
    [SerializeField] private Text _TxtTurnInfo = default;
    [SerializeField] private Text _TxtGameMode = default;
    [SerializeField] private Text _TxtTouchPos = default;
    [SerializeField] private Text _TxtMaster = default;
    [SerializeField] private Text _TxtInitPos = default;

    public static DebugView Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Update()
    {
        if (!GameManager.Instance.IsDebug)
        {
            return;
        }

        _TxtField.text = "";
        for (int y = 0; y < FieldConstant.FIELD_SIZE; y++)
        {
            for (int x = 0; x < FieldConstant.FIELD_SIZE; x++)
            {
                _TxtField.text += ((int)_GameView.Field[y, x]).ToString();
            }
            _TxtField.text += "\n";
        }

        // マウスクリック
        if (Input.GetMouseButtonDown(0))
        {
            int cx, cy;
            _GameView.GetTileIndexFromMousePos(out cx, out cy);
            _TxtTouchPos.text = cx.ToString() + "," + cy.ToString();
        }

        // ターン情報
        _TxtTurnInfo.text = string.Format($"MyIdx:{GameManager.Instance.MyPlayer} Turn:{_GameView.Turn}");

        _TxtMaster.text = "Master:" + PhotonManager.Instance.IsMasterClient().ToString();

        _TxtInitPos.text = "InitPos:" + GameManager.Instance.InitPos.ToString();
    }

    public void SetMtchingStateMsg(string msg)
    {
        _TxtMatching.text = msg;
    }

    public void ShowGameMode()
    {
        _TxtGameMode.text = "GameMode:" + GameManager.Instance.GameMode.ToString();
    }
}
