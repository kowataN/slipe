﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class MatchingView : MonoBehaviourPunCallbacks
{
    [SerializeField] private PhotonView _PhotonView;

    [SerializeField] private InputField _RoomInput = default;
    [SerializeField] private Button _BtnSearch = default;
    [SerializeField] private Button _BtnBack = default;
    [SerializeField] private Button _BtnSetting = default;
    [SerializeField] private Button _BtnHelp = default;

    [Header("MatchgPanel")]
    [SerializeField] private GameObject _MatchingPanel = default;
    [SerializeField] private Text _MatchText = default;
    [SerializeField] private Button _BtnNext = default;
    [SerializeField] private Text _StartBtnText = default;

    [SerializeField] private bool _IsMatching = false;

    private RoomOptions _RoomOptions = new RoomOptions();

    private void Start()
    {
        _MatchingPanel.SetActive(false);
        SetEnableNextBtn(false);

        _RoomOptions.IsVisible = false;
        _RoomOptions.MaxPlayers = 2;
    }

    /// <summary>
    /// サーバーに接続完了
    /// </summary>
    public override void OnConnectedToMaster()
    {
        //DebugView.Instance.SetMtchingStateMsg("OnConnectedToMaster");

        if (_IsMatching)
        {
            CreateRoom();
        }
    }

    private void CreateRoom()
    {
        Debug.Log("CreateRoom");
        PhotonNetwork.JoinOrCreateRoom(_RoomInput.text, _RoomOptions, TypedLobby.Default);
    }

    /// <summary>
    /// 入室
    /// </summary>
    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");
        //DebugView.Instance.SetMtchingStateMsg("OnJoinedRoom");

        SetMatchingPanel();

        if (PhotonNetwork.PlayerList.Length == 2)
        {
            _MatchText.text = "対戦開始待ちです。";
        }
    }

    /// <summary>
    /// 退室
    /// </summary>
    public override void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom");
        //DebugView.Instance.SetMtchingStateMsg("OnLeftRoom");

        SetEnableNextBtn(false);
    }

    /// <summary>
    /// 他のプレイヤーが入室
    /// </summary>
    /// <param name="newPlayer"></param>
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        //DebugView.Instance.SetMtchingStateMsg("OnPlayerEnteredRoom");

        _MatchText.text = "対戦相手が見つかりました。";

        // 対戦開始ボタンを押せるようにする
        if (PhotonNetwork.IsMasterClient)
        {
            SetEnableNextBtn(true);
        }
    }

    /// <summary>
    /// 他のプレイヤーが退室
    /// </summary>
    /// <param name="otherPlayer"></param>
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        //DebugView.Instance.SetMtchingStateMsg("OnPlayerLeftRoom");

        _MatchText.text = "対戦相手を探しています。";

        SetEnableNextBtn(false);
    }

    private int GetOrderPlayer()
    {
        return Random.Range(0, 2);
    }

    /// <summary>
    /// マッチングボタン押下処理
    /// </summary>
    public void OnClickMatching()
    {
        Debug.Log("OnClickMatching");

        if (_RoomInput.text == "" || _RoomInput.text.Length != 4)
        {
            SoundManager.Instance.PlayCancel();
            return;
        }

        _IsMatching = true;

        _BtnSearch.interactable = false;
        _BtnBack.interactable = false;
        _BtnSetting.interactable = false;
        _BtnHelp.interactable = false;

        SoundManager.Instance.PlayTap();

        _MatchText.text = "対戦相手を探しています。";

        // マッチング開始
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
            if (string.IsNullOrEmpty(PhotonNetwork.NickName))
            {
                PhotonNetwork.NickName = "Player_" + Random.Range(1, 999999);
            }
        }
        else
        {
            CreateRoom();
        }
    }

    /// <summary>
    /// 戻るボタン押下処理
    /// </summary>
    public void OnClickBack()
    {
        //DebugView.Instance.SetMtchingStateMsg(" ");

        _IsMatching = false;
        SetMatchingPanel();

        Debug.Log("MatchingView::OnClickBack connect:" + PhotonNetwork.IsConnected.ToString());
        PhotonNetwork.Disconnect();
    }

    private void SetMatchingPanel()
    {
        _MatchingPanel.SetActive(_IsMatching);
    }

    public void OnClickNext()
    {
        GameManager.Instance.GameMode = GameManager.GameMainMode.kMulti;
        if (PhotonNetwork.IsMasterClient)
        {
            int playerIndex = GetOrderPlayer();
            GameManager.Instance.SetMyPlayerIndex(playerIndex);

            PhotonManager.Instance.SendSetOrderPlayer(1 - playerIndex);
            PhotonManager.Instance.SendSetSceneView(GameManager.SceneType.kInitPos);
        }
    }

    public void OnClickCancel()
    {
        SoundManager.Instance.PlayCancel();

        _IsMatching = false;

        // マッチングをやめる
        SetMatchingPanel();
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.LeaveRoom();
        }

        _BtnSearch.interactable = true;
        _BtnBack.interactable = true;
        _BtnSetting.interactable = true;
        _BtnHelp.interactable = true;
    }

    private void SetEnableNextBtn(bool value)
    {
        _BtnNext.interactable = value;
        _StartBtnText.color = value ? Color.white : Color.gray;
    }
}
