﻿using UnityEngine;
using UnityEngine.UI;

public class BlockView : MonoBehaviour
{
    [SerializeField] private Image _Image;

    public void SetSprite(Sprite spr)
    {
        _Image.sprite = spr;
    }
}