﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitPosView : MonoBehaviour
{
    [SerializeField] private SpriteRenderer[] _Icons = default;
    [SerializeField] private Button _BtnStart = default;
    [SerializeField] private Text _TxtStart = default;

    private void Start()
    {
        SetImageScale(0.5f);
    }

    public void OnClickButton(int index)
    {
        SoundManager.Instance.PlayTap();

        PhotonManager.Instance.SendSetInitPos(index);
    }

    public void OnClickBack()
    {
        GameManager.Instance.SetSceneView(GameManager.SceneType.kTitle);
    }

    public void OnClickStart()
    {
        // ゲーム開始
        PhotonManager.Instance.SendSetSceneView(GameManager.SceneType.kMain);
    }

    private void SetImageScale(float scale)
    {
        _Icons[GameManager.Instance.InitPos].transform.localScale = new Vector3(scale, scale, 1);
    }

    public void UpdateCrtIndex(int index)
    {
        SetImageScale(0.3f);
        GameManager.Instance.InitPos = index;
        SetImageScale(0.5f);
    }

    public void SetEnableNextBtn(bool value)
    {
        _BtnStart.interactable = value;
        _TxtStart.color = value ? Color.white : Color.gray;
    }
}
