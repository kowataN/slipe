﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnPlayerView : MonoBehaviour
{
    [SerializeField] private Animator _Animator = default;
    [SerializeField] private Text _Text = default;

    private void Awake()
    {
        SetActiveState(0);
    }

    public void PlayerAnimation(string msg)
    {
        Debug.Log("PlayerAnimation : " + msg);
        _Text.text = msg;
        // カメラが反転しているので、文字も反転させる
        _Text.transform.rotation = Quaternion.Euler(0.0f, 0.0f, GameManager.Instance.MyPlayer * 180.0f);
        _Animator.Play("ShowPlayer", 0, 0.0f);
        this.gameObject.SetActive(true);
    }

    private void SetActiveState(int value)
    {
        Debug.Log("SetActiveState:" + value.ToString());
        gameObject.SetActive(value == 1 ? true : false);
    }
}
