﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingView : MonoBehaviour
{
    [SerializeField] private Slider _BGMSlider = default;
    [SerializeField] private Slider _SESlider = default;

    private void Start()
    {
        float bgmVol = SoundManager.Instance.GetBGMVolume();
        _BGMSlider.value = bgmVol * 100.0f * 5.0f;

        float seVol = SoundManager.Instance.GetSEVolume();
        _SESlider.value = seVol * 100.0f;
    }

    public void OnClickBack()
    {
        // 音量を保存
        SaveDataManager.Instance.SaveAudio();

        GameManager.Instance.SetSceneView(GameManager.SceneType.kTitle);
    }

    public void OnChangeValueBGM()
    {
        SoundManager.Instance.SetVolumeBGM(_BGMSlider.value / 100.0f / 5.0f);
    }

    public void OnChangeValueSE()
    {
        SoundManager.Instance.SetVolumeSE(_SESlider.value / 100.0f);
    }

    public void OnClickConfirm()
    {
     }
}
