﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleView : MonoBehaviour
{
    [SerializeField] private GameObject _GameModeParent = default;
    [SerializeField] private GameObject _MatchingParent = default;


    private void Start()
    {
        // 起動時に音量を読み込む
        SaveDataManager.Instance.LoadAudio();

        SetSubType(TitleSubType.kPlayModeSelect);
    }

    public void OnClickSolo()
    {
        GameManager.Instance.SetSceneView(GameManager.SceneType.kInitPos);
    }

    public void OnClickMulti()
    {
        //Debug.Log("OnClickMulti");
        SetSubType(TitleSubType.kMactching);
    }

    public void OnClickHelp()
    {
        GameManager.Instance.SetSceneView(GameManager.SceneType.kHelp);
    }

    public void OnClickSetting()
    {
        GameManager.Instance.SetSceneView(GameManager.SceneType.kSetting);
    }

    public enum TitleSubType
    {
        kPlayModeSelect, kMactching
    };
    private TitleSubType _crtSubType = TitleSubType.kPlayModeSelect;

    private void SetSubType(TitleSubType type)
    {
        _crtSubType = type;

        _GameModeParent.SetActive(_crtSubType == TitleSubType.kPlayModeSelect);
        _MatchingParent.SetActive(_crtSubType == TitleSubType.kMactching);
    }
    public void OnClickBack()
    {
        SetSubType(TitleSubType.kPlayModeSelect);
    }
}
