﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static GameManager;
using Photon.Pun;
using Photon.Realtime;

public partial class GameView : MonoBehaviour
{
    public enum TileVec
    {
        kUp, kDown, kLeft, kRight
    }

    // ターン
    // 0:白番、1:黒番
    public int Turn { get; set; } = 0;

    private Dictionary<int, List<FieldConstant.TileType>> _CanSetupTile = new Dictionary<int, List<FieldConstant.TileType>>();

    public const float FIELD_SIZE_HALF = FieldConstant.FIELD_SIZE / 2.0f;
    public const float TILE_SIZE = 64.0f*2;
    public const float TILE_SIZE_HALF = TILE_SIZE / 2.0f;

    //[Range(0, 4)]
    //[SerializeField] private int _InitPos = 0;

    [SerializeField] private GameObject _TilePrefab = default;
    [SerializeField] private Transform _TilesTransform = default;
    [SerializeField] private GameObject _OperationPanel = default;

    [Header("Win Panel")]
    [SerializeField] private GameObject _WinPanel = default;
    [SerializeField] private Text _WinPlayerText = default;

    [SerializeField] private GameObject _TurnPlayerPanel = default;
    private TurnPlayerView _TurnPlayerView = default;

    [Header("Sprites")]
    [SerializeField] private Sprite[] _Sprites = default;

    private GameObject[,] _Field = new GameObject[FieldConstant.FIELD_SIZE, FieldConstant.FIELD_SIZE];

    private FieldConstant.TileType[,] _FieldState = new FieldConstant.TileType[FieldConstant.FIELD_SIZE, FieldConstant.FIELD_SIZE];
    public FieldConstant.TileType[,] Field { get { return _FieldState; } }

    private OperationView _OperationView = default;

    private bool isEnd = false;

    // Start is called before the first frame update
    void Start()
    {
        _CanSetupTile.Add(0, new List<FieldConstant.TileType> { FieldConstant.TileType.kPlayer1Tile, FieldConstant.TileType.kPlayer1Mark });
        _CanSetupTile.Add(1, new List<FieldConstant.TileType> { FieldConstant.TileType.kPlayer2Tile, FieldConstant.TileType.kPlayer2Mark });

        _OperationView = _OperationPanel.GetComponentInChildren<OperationView>();
        SetOperationButton();

        _TurnPlayerView = _TurnPlayerPanel.GetComponentInChildren<TurnPlayerView>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.CrtScene == SceneType.kMain)
        {
            // マウスクリック
            if (Input.GetMouseButtonDown(0))
            {
                // ゲーム終了していたら何もしない
                if (isEnd)
                {
                    return;
                }

                // 自分の手番ではないので何もしない
                if (!GameManager.Instance.IsMyTurn())
                {
                    return;
                }

                int cx, cy;
                GetTileIndexFromMousePos(out cx, out cy);
                PhotonManager.Instance.SendShowOperationPanel(cx, cy);
            }

            if (GameManager.Instance.IsDebug)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    GameManager.Instance.SetSceneView(SceneType.kInitPos);
                }
            }
        }
    }

    public void RemoveTileObject()
    {
        for (int y = 0; y < FieldConstant.FIELD_SIZE; y++)
        {
            for (int x = 0; x < FieldConstant.FIELD_SIZE; x++)
            {
                Destroy(_Field[y, x]);
            }
        }
    }

    public void ShowOperationView(int x, int y)
    {
        if (CehckFieldOutOfRange(x, y))
        {
            Debug.Log("範囲外");
            return;
        }

        // そこに自信のタイルがあるか
        var canTileList = _CanSetupTile[Turn];
        var targetTile = _FieldState[y, x];
        int targetCt = canTileList.Count(tile => tile == targetTile);
        if (targetCt == 0)
        {
            return;
        }

        if (_OperationPanel.activeSelf)
        {
            return;
        }

        SoundManager.Instance.PlayTap();

        // 操作パネル表示
        var obj = _Field[y, x];
        _OperationPanel.transform.localPosition = obj.transform.localPosition;
        SetActiveOperationPanel(true);
        _OperationView.PositionX = x;
        _OperationView.PositionY = y;

        _OperationView.ButtonCenter.gameObject.SetActive(GameManager.Instance.IsMyTurn());
        if (GameManager.Instance.IsMyTurn())
        {
            // 進む先がない場合は押せないようにする
            FieldConstant.TileType tileType = _FieldState[y, x];
            _OperationView.ButtonUp.interactable = CheckMovableTileVec(TileVec.kUp, tileType, x, y);
            _OperationView.ButtonDown.interactable = CheckMovableTileVec(TileVec.kDown, tileType, x, y);
            _OperationView.ButtonLeft.interactable = CheckMovableTileVec(TileVec.kLeft, tileType, x, y);
            _OperationView.ButtonRight.interactable = CheckMovableTileVec(TileVec.kRight, tileType, x, y);
        }
        else
        {
            _OperationView.ButtonUp.interactable = false;
            _OperationView.ButtonDown.interactable = false;
            _OperationView.ButtonLeft.interactable = false;
            _OperationView.ButtonRight.interactable = false;
        }
    }

    /// <summary>
    /// 指定方向にタイルが置けるかを調べる
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private bool CheckMovableTileVec(TileVec vec, FieldConstant.TileType type, int x, int y)
    {
        bool bRet = false; // 最終判定結果
        bool res = false;
        int targetX = x, targetY = y;
        int start = 0;
        switch (vec)
        {
            // 上方向を調べていく
            case TileVec.kUp:
                start = y - 1;
                for (int h = start; h >= 0; h--)
                {
                    res = CheckMovableTileOne(x, h);
                    if (res)
                    {
                        targetX = x;
                        targetY = h;
                    }
                    else
                    {
                        break;
                    }
                }
                break;
            case TileVec.kDown:
                start = y + 1;
                for (int h = start; h < FieldConstant.FIELD_SIZE; h++)
                {
                    res = CheckMovableTileOne(x, h);
                    if (res)
                    {
                        targetX = x;
                        targetY = h;
                    }
                    else
                    {
                        break;
                    }
                }
                break;
            case TileVec.kLeft:
                start = x - 1;
                for (int w = start; w >= 0; w--)
                {
                    res = CheckMovableTileOne(w, y);
                    if (res)
                    {
                        targetX = w;
                        targetY = y;
                    }
                    else
                    {
                        break;
                    }
                }
                break;
            case TileVec.kRight:
                start = x + 1;
                for (int w = start; w < FieldConstant.FIELD_SIZE; w++)
                {
                    res = CheckMovableTileOne(w, y);
                    if (res)
                    {
                        targetX = w;
                        targetY = y;
                    }
                    else
                    {
                        break;
                    }
                }
                break;
            default:
                break;
        }

        if (JudgeCenter(targetX, targetY) && IsTileNormal(type))
        {
            // そもそもおけない
            return false;
        }

        bRet = _FieldState[targetY, targetX] == FieldConstant.TileType.kBGTile;

        return bRet;

    }

    /// <summary>
    /// 座標がフィールドの範囲外かを返します
    /// </summary>
    /// <param name="x">x座標</param>
    /// <param name="y">y座標</param>
    /// <returns></returns>
    private bool CehckFieldOutOfRange(int x, int y)
    {
        return x < 0 || x >= FieldConstant.FIELD_SIZE || y < 0 || y >= FieldConstant.FIELD_SIZE;
    }

    /// <summary>
    /// 一つ隣のタイルを調べる
    /// </summary>
    /// <param name="type">動かしたいタイルの種類</param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private bool CheckMovableTileOne(int x, int y)
    {
        if (CehckFieldOutOfRange(x, y))
        {
            return false;
        }
        return _FieldState[y, x] == FieldConstant.TileType.kBGTile;
    }

    public void InitField()
    {
        //Debug.Log("InitFiled");
        isEnd = false;
        Turn = 0;

        int initPos = GameManager.Instance.InitPos;
        for (int y = 0; y < FieldConstant.FIELD_SIZE; y++)
        {
            for (int x = 0; x < FieldConstant.FIELD_SIZE; x++)
            {
                _FieldState[y, x] = FieldConstant.FieldInit[initPos, y, x];
            }
        }

        // デバッグ機能
        if (GameManager.Instance.IsDebug)
        {
            //_FieldState[3, 2] = FieldConstant.TileType.kPlayer1Mark;
            //_FieldState[1, 2] = FieldConstant.TileType.kPlayer2Mark;
        }

        SetActiveOperationPanel(false);
        _WinPanel.SetActive(false);
    }

    public void CreateBlockObject()
    {
        //Debug.Log("CreateBlockObject");
        for (int y = 0; y < FieldConstant.FIELD_SIZE; y++)
        {
            for (int x = 0; x < FieldConstant.FIELD_SIZE; x++)
            {
                float posX = x * 64;
                float posY = -y * 64;
                GameObject newBlock = GameObject.Instantiate<GameObject>(_TilePrefab);
                newBlock.transform.parent = _TilesTransform.transform;
                newBlock.transform.localPosition = new Vector3(posX, posY, 0.0f);
                _Field[y, x] = newBlock;
                BlockView blockview = newBlock.GetComponentInChildren<BlockView>();
                if (blockview == null)
                {
                    Debug.LogError("BlockView is null");
                }
                blockview.SetSprite(_Sprites[(int)_FieldState[y, x]]);
            }
        }
    }

    public void GetTilePosition(int x, int y, out float posX, out float posY)
    {
        posX = (x - FIELD_SIZE_HALF) * TILE_SIZE + TILE_SIZE_HALF;
        posY = -(y - FIELD_SIZE_HALF) * TILE_SIZE - TILE_SIZE_HALF;
    }

    public void GetTileIndexFromMousePos(out int x, out int y)
    {
        Vector2 mousePos = Input.mousePosition;
        float resX = mousePos.x / TILE_SIZE;
        float resY = mousePos.y / TILE_SIZE;
        x = resX < 0 ? -1 : (int)resX;
        y = resY < 0 ? -1 : (int)resY;
        y = (FieldConstant.FIELD_SIZE - 1) - y; // y軸は反転させる

        // 自身が黒番の場合は座標を反転させる
        if (GameManager.Instance.MyPlayer == 1)
        {
            x = (FieldConstant.FIELD_SIZE - 1) - x;
            y = (FieldConstant.FIELD_SIZE - 1) - y;
        }
    }

    private void SetOperationButton()
    {
        _OperationView.OnClickAction += new System.Action<int, int>(OnClickOperationButton);
    }

    private void OnClickOperationButton(int addX, int addY)
    {
        if (!GameManager.Instance.IsMyTurn())
        {
            return;
        }

        if (addX == -1 && addY == -1)
        {
            PhotonManager.Instance.SendInvisibleOperationPanel();
            return;
        }

        SoundManager.Instance.PlayTap();

        // 矢印押下：再帰的に移動可能かを調べタイルをすすめる
        var baseX = _OperationView.PositionX;
        var baseY = _OperationView.PositionY;
        var targetX = baseX;
        var targetY = baseY;

        // TODO: 後で関数化
        bool isMovable = true;
        while (isMovable)
        {
            isMovable = CheckMovableTileOne(targetX + addX, targetY + addY);
            if (!isMovable)
            {
                // タイルを入れ替える
                PhotonManager.Instance.SendSwapTile(targetX, targetY, baseX, baseY);
                break;
            }
            targetX += addX;
            targetY += addY;
        }
    }

    IEnumerator ShowWinEffect()
    {
        yield return null;
        yield return new WaitForSeconds(0.5f);
        _WinPanel.transform.rotation = Quaternion.Euler(0.0f, 0.0f, GameManager.Instance.MyPlayer * 180.0f);
        _WinPanel.SetActive(true);

        yield break;
    }

    public void DrawTile()
    {
        for (int y = 0; y < FieldConstant.FIELD_SIZE; y++)
        {
            for (int x = 0; x < FieldConstant.FIELD_SIZE; x++)
            {
                BlockView blockview = _Field[y, x].GetComponentInChildren<BlockView>();
                if (blockview == null)
                {
                    Debug.LogError("BlockView is null");
                }
                blockview.SetSprite(_Sprites[(int)_FieldState[y, x]]);
            }
        }
    }

    /// <summary>
    /// 中央か
    /// </summary>
    /// <returns></returns>
    private bool JudgeCenter(int x, int y) { return x == 2 && y == 2; }

    private bool IsTileNormal(FieldConstant.TileType type)
    {
        return (type == FieldConstant.TileType.kPlayer1Tile || type == FieldConstant.TileType.kPlayer2Tile);
    }

    public void OnEndButton()
    {
        SoundManager.Instance.PlayTap();

        GameManager.Instance.SetSceneView(SceneType.kTitle);    
    }

    public void SwapTile(int srcX, int srcY, int dstX, int dstY)
    {
        FieldConstant.TileType srcTile = _FieldState[srcY, srcX];
        _FieldState[srcY, srcX] = _FieldState[dstY, dstX];
        _FieldState[dstY, dstX] = srcTile;

        DrawTile();

        // 終了判定
        if (JudgeCenter(srcX, srcY))
        {
            isEnd = true;
            _WinPlayerText.text = string.Format($"プレイヤー{Turn + 1}の勝利 !!");
            StartCoroutine("ShowWinEffect");
        }
        else
        {

            PhotonManager.Instance.SendUpdateTurn(1 - Turn);
        }
    }

    public void SetActiveOperationPanel(bool value)
    {
        _OperationPanel.SetActive(value);
    }

    public void ShowTurnPlayerAnimation()
    {
        // ソロとマルチ文言を変える
        if (GameManager.Instance.GameMode == GameMainMode.kSolo)
        {
            _TurnPlayerView.PlayerAnimation(Turn == 0 ? "白番です。" : "黒番です。");
        }
        else
        {
            _TurnPlayerView.PlayerAnimation(GameManager.Instance.IsMyTurn() ? "あなたの番です。" : "相手の番です。");
        }
    }

    public void ShowFirstTurnAnimation()
    {
        StartCoroutine("ShowFirstAnimationEnum");
    }

    IEnumerator ShowFirstAnimationEnum()
    {
        yield return null;
        yield return new WaitForSeconds(0.5f);
        ShowTurnPlayerAnimation();
        yield break;
    }
}
