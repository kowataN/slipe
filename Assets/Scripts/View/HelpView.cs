﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpView : MonoBehaviour
{
    [Header("Next")]
    [SerializeField] private Button _BtnNext = default;

    [Header("Back")]
    [SerializeField] private Button _BtnBack = default;

    [Header("Objects")]
    [SerializeField] private GameObject[] _Objects = default;

    /// <summary>
    /// 遊び方のページ数
    /// </summary>
    private int _CrtIndex = 0;


    public void Init()
    {
        foreach (GameObject obj in _Objects)
        {
            obj.SetActive(false);
        }
        _CrtIndex = 0;
        ShowHelpObject(_CrtIndex);
    }

    public void OnClickNext()
    {
        ShowHelpObject(_CrtIndex + 1);
    }

    public void OnClickBack()
    {
        ShowHelpObject(_CrtIndex - 1);
    }

    public void OnClickTitle()
    {
        GameManager.Instance.SetSceneView(GameManager.SceneType.kTitle);
    }

    private void ShowHelpObject(int index)
    {
        _Objects[_CrtIndex].SetActive(false);
        index = (int)Mathf.Clamp(index, 0, _Objects.Length);
        _CrtIndex = index;
        _Objects[_CrtIndex].SetActive(true);

        SetNextButtonEnabled(_CrtIndex != _Objects.Length-1);
        SetBackButtonEnabled(_CrtIndex != 0);
    }

    private void SetNextButtonEnabled(bool value)
    {
        _BtnNext.interactable = value;
    }

    private void SetBackButtonEnabled(bool value)
    {
        _BtnBack.interactable = value;
    }
}
