﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class OperationView : MonoBehaviour
{
    [SerializeField] private Button _ButtonUp = default;
    [SerializeField] private Button _ButtonDown = default;
    [SerializeField] private Button _ButtonLeft = default;
    [SerializeField] private Button _ButtonRight = default;
    [SerializeField] private Button _ButtonCenter = default;

    [SerializeField] private int _PositionX;
    [SerializeField] private int _PositionY;

    public Button ButtonUp { get { return _ButtonUp; } }
    public Button ButtonDown { get { return _ButtonDown; } }
    public Button ButtonLeft { get { return _ButtonLeft; } }
    public Button ButtonRight { get { return _ButtonRight; } }
    public Button ButtonCenter { get { return _ButtonCenter; } }

    public int PositionX
    {
        get { return _PositionX; }
        set { _PositionX = value; }
    }
    public int PositionY
    {
        get { return _PositionY; }
        set { _PositionY = value; }
    }

    public Action<int, int> OnClickAction { get; set; }

    public void OnClickArrow(int type)
    {
        if (type == 99)
        {
            SoundManager.Instance.PlayCancel();
            OnClickAction?.Invoke(-1, -1);
            return;
        }

        int addX = 0, addY = 0;
        switch (type)
        {
            // 上下
            case -1:
            case 1:
                addY = type;
                break;
            case 4:
            case 6:
                addX = type - 5;
                break;
            default:
                addX = addY = 0;
                break;
        }

        SoundManager.Instance.PlayTap();
        OnClickAction?.Invoke(addX, addY);
    }
}
