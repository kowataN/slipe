﻿public class LockManager : SingletonBase<LockManager> {
    private int _lockCount;

    public void Lock() {
        ++_lockCount;
    }

    public void UnLock() {
        --_lockCount;
    }

    public bool IsLock() {
        return _lockCount != 0;
    }

    public bool IsUnLock() {
        return _lockCount == 0;
    }
}
