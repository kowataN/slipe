using UnityEngine;
using System.Collections;

public class SingletonMonoBhv<T> : MonoBehaviour where T : MonoBehaviour {
    private static T _Instance;
    public static T Instance {
        get {
            if (_Instance == null) {
                _Instance = (T)FindObjectOfType(typeof(T));
                if (_Instance == null) {
                    // 見つからなかった場合
                    GameObject obj = new GameObject();
                    _Instance = obj.AddComponent<T>();
                    obj.name = typeof(T).ToString();
                    DontDestroyOnLoad(obj);
                }
            }
            return _Instance;
        }
    }
}
