﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    [SerializeField] AudioClip _Tap = default;
    [SerializeField] AudioClip _Cancel = default;

    [SerializeField] private AudioSource _BGMAS = default;
    [SerializeField] private AudioSource _SEAS = default;

    private float _backupSEVolume = 0.0f;
    private float _backupBGMVolume = 0.0f;

    public void PlayTap()
    {
        _SEAS.PlayOneShot(_Tap);
    }
    public void PlayCancel()
    {
        _SEAS.PlayOneShot(_Cancel);
    }

    public void SetVolumeSE(float vol)
    {
        _SEAS.volume = vol;
    }

    public void SetVolumeBGM(float vol)
    {
        _BGMAS.volume = vol;
    }

    public void BackupVolume()
    {
        _backupSEVolume = _SEAS.volume;
        _backupBGMVolume = _BGMAS.volume;
    }

    public void RevertBckupData()
    {
        _SEAS.volume = _backupSEVolume;
        _BGMAS.volume = _backupBGMVolume;
    }

    /// <summary>
    /// 現在のSEの音量を返します。
    /// </summary>
    /// <returns>SE音量</returns>
    public float GetSEVolume() => _SEAS.volume;

    /// <summary>
    /// 現在のBGMの音量を返します。
    /// </summary>
    /// <returns>BGM音量</returns>
    public float GetBGMVolume() => _BGMAS.volume;
}
