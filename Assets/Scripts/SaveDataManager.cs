﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveDataManager : SingletonMonoBhv<SaveDataManager>
{
    /// <summary>
    /// 音量データを保存します。
    /// </summary>
    public void SaveAudio()
    {
        PlayerPrefs.SetFloat("VOL_SE", SoundManager.Instance.GetSEVolume());
        PlayerPrefs.SetFloat("VOL_BGM", SoundManager.Instance.GetBGMVolume());

        PlayerPrefs.Save();
    }

    /// <summary>
    /// 音量データを読み込みます。
    /// </summary>
    public void LoadAudio()
    {
        float seVol = 0.5f;
        if (PlayerPrefs.HasKey("VOL_SE"))
        {
            seVol = PlayerPrefs.GetFloat("VOL_SE");
        }

        float bgmVol = 0.1f;
        if (PlayerPrefs.HasKey("VOL_BGM"))
        {
            bgmVol = PlayerPrefs.GetFloat("VOL_BGM");
        }

        SoundManager.Instance.SetVolumeSE(seVol);
        SoundManager.Instance.SetVolumeBGM(bgmVol);
    }
}
