﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [SerializeField] private GameObject _MainCamera = default;

    [SerializeField] private GameView _GameView = default;
    [SerializeField] private InitPosView _InitPosView = default;
    [SerializeField] private HelpView _HelpView = default;
    [SerializeField] private bool _IsDebug = true;

    public bool IsDebug { get { return _IsDebug; } }

    public int InitPos { get; set; }

    [SerializeField] private GameObject _ViewParent = default;

    public enum SceneType
    {
        kTitle, kInitPos, kMain, kSetting, kHelp
    }

    public enum GameMainMode
    {
        kSolo, kMulti
    }

    private GameMainMode _GameMode = GameMainMode.kSolo;
    public GameMainMode GameMode
    {
        set
        {
            this._GameMode = value;
            //DebugView.Instance.ShowGameMode();
        }
        get
        {
            return this._GameMode;
        }
    }

    /// <summary>
    /// 自身のプレイヤーインデックス
    /// </summary>
    [SerializeField] private int _MyPlayerIndex = 0;
    public int MyPlayer { get { return _MyPlayerIndex; } }

    private Dictionary<SceneType, float> _ScenePos = new Dictionary<SceneType, float>();

    private SceneType _CrtScene = SceneType.kTitle;
    public SceneType CrtScene { get { return _CrtScene; } }
    public enum MatchingState
    {
        kIdle, kMatching
    };
    // TODO:
    //private MatchingState _MatchingState = MatchingState.kIdle;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        _ScenePos.Add(SceneType.kTitle, 0.0f * _ViewParent.transform.localScale.x);
        _ScenePos.Add(SceneType.kInitPos, -320.0f * _ViewParent.transform.localScale.x);
        _ScenePos.Add(SceneType.kMain, -640.0f * _ViewParent.transform.localScale.x);
        _ScenePos.Add(SceneType.kSetting, -960.0f * _ViewParent.transform.localScale.x);
        _ScenePos.Add(SceneType.kHelp, -1280.0f * _ViewParent.transform.localScale.x);
    }

    /// <summary>
    /// 表示する画面を設定します。
    /// </summary>
    /// <param name="scene"></param>
    public void SetSceneView(SceneType scene)
    {
        _CrtScene = scene;
        float posX = _ScenePos[scene];
        _ViewParent.transform.localPosition = new Vector3(posX, 0.0f, 0.0f);

        if (_CrtScene == SceneType.kInitPos)
        {
            ResetMainCameraRotate();
            if (GameMode == GameMainMode.kMulti)
            {
                _InitPosView.SetEnableNextBtn(PhotonNetwork.IsMasterClient);
            }
        }
        else if (_CrtScene == SceneType.kTitle)
        {
            ResetMainCameraRotate();
        }
        else if (_CrtScene == SceneType.kMain)
        {
            // 対戦準備
            _GameView.InitField();
            _GameView.RemoveTileObject();
            _GameView.CreateBlockObject();

            SetMainCameraRotate();

            _GameView.ShowFirstTurnAnimation();
        }
        else if (_CrtScene == SceneType.kSetting)
        {
            SoundManager.Instance.BackupVolume();
            ResetMainCameraRotate();
        }
        else if (_CrtScene == SceneType.kHelp)
        {
            _HelpView.Init();
        }
    }

    public void SetMyPlayerIndex(int index)
    {
        _MyPlayerIndex = index;
    }

    public bool IsMyTurn()
    {
        if (GameMode == GameMainMode.kSolo)
        {
            return true;
        }
        return MyPlayer == _GameView.Turn;
    }

    public void ResetMainCameraRotate()
    {
        _MainCamera.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
    }

    public void SetMainCameraRotate()
    {
        _MainCamera.transform.rotation = Quaternion.Euler(0.0f, 0.0f, _MyPlayerIndex * 180.0f);

    }
}
