﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FieldConstant
{
    public enum TileType
    {
        kBGTile, kPlayer1Tile, kPlayer2Tile, kPlayer1Mark, kPlayer2Mark
    }

    public const int MAX_FIELD_INIT_POS = 5;
    public const int FIELD_SIZE = 5;

    public static readonly TileType[,,] FieldInit = new TileType[MAX_FIELD_INIT_POS, FIELD_SIZE, FIELD_SIZE]
    {
        {
            { TileType.kPlayer2Tile, TileType.kPlayer2Tile, TileType.kPlayer2Mark, TileType.kPlayer2Tile, TileType.kPlayer2Tile},
            { TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile},
            { TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile},
            { TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile},
            { TileType.kPlayer1Tile, TileType.kPlayer1Tile, TileType.kPlayer1Mark, TileType.kPlayer1Tile, TileType.kPlayer1Tile},
        },
        {
            { TileType.kPlayer2Tile, TileType.kBGTile, TileType.kPlayer2Mark, TileType.kBGTile, TileType.kPlayer2Tile},
            { TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile},
            { TileType.kPlayer1Tile, TileType.kPlayer2Tile, TileType.kBGTile, TileType.kPlayer1Tile, TileType.kPlayer2Tile},
            { TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile},
            { TileType.kPlayer1Tile, TileType.kBGTile, TileType.kPlayer1Mark, TileType.kBGTile, TileType.kPlayer1Tile},
        },
        {
            { TileType.kPlayer2Tile, TileType.kPlayer2Mark, TileType.kBGTile, TileType.kBGTile, TileType.kPlayer1Tile},
            { TileType.kPlayer2Tile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile},
            { TileType.kPlayer1Tile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kPlayer2Tile},
            { TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kPlayer1Tile},
            { TileType.kPlayer2Tile, TileType.kBGTile, TileType.kBGTile, TileType.kPlayer1Mark, TileType.kPlayer1Tile},
        },
        {
            { TileType.kPlayer1Tile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kPlayer2Mark},
            { TileType.kBGTile, TileType.kBGTile, TileType.kPlayer2Tile, TileType.kPlayer2Tile, TileType.kBGTile},
            { TileType.kBGTile, TileType.kPlayer1Tile, TileType.kBGTile, TileType.kPlayer2Tile, TileType.kBGTile},
            { TileType.kBGTile, TileType.kPlayer1Tile, TileType.kPlayer1Tile, TileType.kBGTile, TileType.kBGTile},
            { TileType.kPlayer1Mark, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kPlayer2Tile},
        },
        {
            { TileType.kPlayer2Tile, TileType.kBGTile, TileType.kPlayer1Tile, TileType.kBGTile, TileType.kPlayer2Tile},
            { TileType.kBGTile, TileType.kPlayer1Mark, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile},
            { TileType.kPlayer2Tile, TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kPlayer1Tile},
            { TileType.kBGTile, TileType.kBGTile, TileType.kBGTile, TileType.kPlayer2Mark, TileType.kBGTile},
            { TileType.kPlayer1Tile, TileType.kBGTile, TileType.kPlayer2Tile, TileType.kBGTile, TileType.kPlayer1Tile},
        }
    };
}
