﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class PhotonManager : SingletonMonoBhv<PhotonManager>
{
    [SerializeField] private PhotonView _PhotonView = default;
    [SerializeField] private GameView _GameView = default;
    [SerializeField] private InitPosView _InitPosView = default;

    public bool IsMasterClient()
    {
        if (!PhotonNetwork.IsConnected)
        {
            return false;
        }

        return PhotonNetwork.IsMasterClient;
    }

    public void SendSetOrderPlayer(int index)
    {
        //Debug.Log("SendSetOrderPlayer");
        if (!PhotonNetwork.IsMasterClient)
        {
            return;
        }
        _PhotonView.RPC("PunSetOrderPlayer", RpcTarget.Others, index);
    }

    [PunRPC]
    private void PunSetOrderPlayer(int index)
    {
        //Debug.Log("PunSetOrderPlayer : " + index.ToString());
        GameManager.Instance.SetMyPlayerIndex(index);
        GameManager.Instance.GameMode = GameManager.GameMainMode.kMulti;
    }

    public void SendSetSceneView(GameManager.SceneType type)
    {
        if (GameManager.Instance.GameMode == GameManager.GameMainMode.kSolo)
        {
            PunSetSceneView(type);
            return;
        }
        _PhotonView.RPC("PunSetSceneView", RpcTarget.AllViaServer, type);
    }

    [PunRPC]
    private void PunSetSceneView(GameManager.SceneType type)
    {
        GameManager.Instance.SetSceneView(type);
    }

    public void SendShowOperationPanel(int x, int y)
    {
        if (GameManager.Instance.GameMode == GameManager.GameMainMode.kSolo)
        {
            PunShowOperationPanel(x, y);
            return;
        }

        _PhotonView.RPC("PunShowOperationPanel", RpcTarget.AllViaServer, x, y);
    }

    [PunRPC]
    private void PunShowOperationPanel(int x, int y)
    {
        _GameView.ShowOperationView(x, y);
    }

    public void SendInvisibleOperationPanel()
    {
        if (GameManager.Instance.GameMode == GameManager.GameMainMode.kSolo)
        {
            PunInvisibleOperationPanel();
            return;
        }

        _PhotonView.RPC("PunInvisibleOperationPanel", RpcTarget.AllViaServer);
    }

    [PunRPC]
    private void PunInvisibleOperationPanel()
    {
        _GameView.SetActiveOperationPanel(false);
    }

    public void SendSwapTile(int srcX, int srcY, int dstX, int dstY)
    {
        if (GameManager.Instance.GameMode == GameManager.GameMainMode.kSolo)
        {
            PunSwapTile(srcX, srcY, dstX, dstY);
            return;
        }

        _PhotonView.RPC("PunSwapTile", RpcTarget.AllViaServer, srcX, srcY, dstX, dstY);
    }

    [PunRPC]
    private void PunSwapTile(int srcX, int srcY, int dstX, int dstY)
    {
        //Debug.Log($"src:{srcX},{srcY} dst:{dstX},{dstY}");
        _GameView.SwapTile(srcX, srcY, dstX, dstY);
        _GameView.SetActiveOperationPanel(false);
    }

    public void SendUpdateTurn(int turn)
    {
        if (GameManager.Instance.GameMode == GameManager.GameMainMode.kSolo)
        {
            UpdateTurn(turn);
            return;
        }

        _PhotonView.RPC("UpdateTurn", RpcTarget.AllViaServer, turn);
    }

    [PunRPC]
    private void UpdateTurn(int turn)
    {
        _GameView.Turn = turn;
        _GameView.ShowTurnPlayerAnimation();
    }

    public void SendSetInitPos(int index)
    {
        if (GameManager.Instance.GameMode == GameManager.GameMainMode.kSolo)
        {
            SetInitPos(index);
            return;
        }

        _PhotonView.RPC("SetInitPos", RpcTarget.AllViaServer, index);
    }

    [PunRPC]
    private void SetInitPos(int index)
    {
        _InitPosView.UpdateCrtIndex(index);
    }
}
